from django.conf.urls import patterns, url
from eim.arp.portal.settings import MEDIA_URL, MEDIA_ROOT

urlpatterns = patterns('',
    url(r'^$', 'eim.arp.portal.views.home', name='home'),  
    url(r'^Screen1$', 'eim.arp.portal.views.initial_checks', name='initial_checks'),
    url(r'^RiskParity$', 'eim.arp.portal.views.risk_parity', name='riskparity'),
)

urlpatterns += patterns('django.views.static', (r'^%s(?P<path>.*)$' % (MEDIA_URL[1:],), 'serve', {'document_root':MEDIA_ROOT, 'show_indexes': True}),)
