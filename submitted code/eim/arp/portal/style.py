# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 07:44:22 2015

@author: sjain
"""

import cx_Oracle
import os
import numpy as np, pandas as pd
import xlwt, xlsxwriter, datetime

os.environ["ORACLE_HOME"] ="C:\Oracle\product\instantclient_10_2"
os.environ["LD_LIBRARY_PATH"] ="C:\Oracle\product\instantclient_10_2"
os.environ["TNS_ADMIN"] ="C:\Oracle\product\instantclient_10_2"


HOST = 'sump.eimad.local'                   
PORT = '1521'
SID = 'testdb'

dsn_1 = cx_Oracle.makedsn(HOST, PORT , SID)
con = cx_Oracle.connect('I_JCPLESSIS' ,'JCPLESSIS',dsn_1)
cur = con.cursor()
sql_fund_data = "SELECT f.fund_id, f.fund_lw AS fund_name, SUBSTR(fund_lw, 1, INSTR(fund_lw, ' ')-1) AS provider,"+\
"cf.newstyle AS style, cf.newstrategy AS strategy, cf.newmethodology AS methodology," +\
"cf.newassetclass AS asset_class,cf.newclassgrouplw AS classification_group" +\
" FROM v_arp_funds f,v_eim_classification cf WHERE f.fund_id = cf.fund_id(+)  " 

cur.execute(sql_fund_data)
fund_data = cur.fetchall()

cur.close()
con.close()
