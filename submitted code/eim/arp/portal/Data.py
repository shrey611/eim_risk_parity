def fundnames():
    import cx_Oracle
    import os
    import numpy as np, pandas as pd
    import xlwt, xlsxwriter, datetime
    
    os.environ["ORACLE_HOME"] ="C:\Oracle\product\instantclient_10_2"
    os.environ["LD_LIBRARY_PATH"] ="C:\Oracle\product\instantclient_10_2"
    os.environ["TNS_ADMIN"] ="C:\Oracle\product\instantclient_10_2"
    
    
    HOST = 'sump.eimad.local'                   
    PORT = '1521'
    SID = 'testdb'
    
    dsn_1 = cx_Oracle.makedsn(HOST, PORT , SID)
    con = cx_Oracle.connect('I_JCPLESSIS' ,'JCPLESSIS',dsn_1)
    print con.version
    cur = con.cursor()
    
    sql_fund_names = "SELECT f.fund_id " +\
    "FROM fund f, " +\
    "data_contents dc, " +\
    "data_descriptors dd " +\
    "WHERE dd.id = dc.datadescriptor_id " +\
    "AND dc.entityid = f.fund_id " +\
    "AND dd.alias = 'isARP' " +\
    "AND dc.booleanvalue = '1'  " 
    
    cur.execute(sql_fund_names)
    fund_name = cur.fetchall()
    fund_names=np.array(fund_name)
    
    sql_dates = "select distinct asva.asva_dat from asva,track, fund f, data_contents dc,data_descriptors dd " +\
    "WHERE asva.track_id = track.track_id and track.fund_id = f.fund_id and dd.id = dc.datadescriptor_id " +\
    "AND dc.entityid = f.fund_id " +\
    "AND dd.alias = 'isARP' " +\
    "AND dc.booleanvalue = '1' " +\
    "AND track.track_typ = 'NAV' " +\
    "AND track.track_prio = 0 " +\
    "AND track.track_readonly = 'false' ORDER BY asva.asva_dat asc"
    cur.execute(sql_dates)
    date_a = cur.fetchall()
    date_array=np.array(date_a)
    
    def sql_fund_data (fund_id):
        sql_query = " SELECT ava_Dat, ava_val " +\
            "FROM " +\
            "  (SELECT track.track_id, " +\
            "    COUNT(asva.asva_dat) over (partition BY asva.asva_dat)AS flag , " +\
            "    asva.asva_dat                                         AS ava_dat , " +\
            "    asva.asva_val                                         AS ava_val, " +\
            "    asva.asva_sta                                         AS ava_sta " +\
            "  FROM asva, track " +\
            "  WHERE asva.track_id = track.track_id " +\
            "  AND track.fund_id =" + "'" + fund_id  + "'"" " +\
            "  AND track.track_typ      = 'NAV' " +\
            "  AND track.track_prio     = 0 " +\
            "  AND track.track_readonly = 'false' " +\
            "  ORDER BY asva.asva_dat asc) " +\
            "WHERE flag = 1 " +\
            "OR ava_sta = 'Final'  " 
        return sql_query
    
    
    data_values=np.array([[None]*(fund_names.shape[0])]*(10000)) #empty array for data values
    
    date_values=np.array([[None]*(fund_names.shape[0])]*(10000)) #empty array for data values
    fund_names_l=np.array([[None]*(fund_names.shape[0])])
    
    j = 0
    for i in range(0,fund_names.shape[0]):
        sql_query_exec = sql_fund_data(str(fund_names[i,0]))
        cur.execute(sql_query_exec)
        res = cur.fetchall()
        xx = np.array(res)
        a = xx.shape[0]
        if a > 0:
            j = j+1
            data_values[1:xx.shape[0],j]=xx[1:xx.shape[0],1]  
            date_values [1:xx.shape[0],j] =xx[1:xx.shape[0],0]  
            fund_names_l[0,j-1] = fund_names[i,0]
                   
    #final_data=np.array([[None]*(fund_names.shape[0])]*(10000)) 
     
    
    #    for x in range(1,6): #j+1
    #        for y in range(0,date_array.shape[0]):
    #            loc = np.where(date_values[:,x] == date_array[y,0])
    #            if any(char.isdigit() for char in str(loc).strip('dtype=int32),')):
    #                final_data[y+1 , x] = data_values[loc,x]
    #            else:
    #                final_data[y+1 , x] = final_data[y , x]
    #    
    #fund_names_l=[x for x in fund_names_l if x is not None]    
    
    
    #Generating Output
     
    #row = 0
    #col = 0
    #workbook=xlsxwriter.Workbook('Final.xlsx')
    #worksheet=workbook.add_worksheet('Results')
    #for row, array in enumerate(final_data):   
    #    for col, value in enumerate(array):
    #        worksheet.write(row+7, col+2, value)
    #
    #for col in range(0, fund_names_l.shape[1]):
    #    worksheet.write(0+6, col+3,  fund_names_l[0,col]) 
    #      
    #worksheet=workbook.add_worksheet('Dates')
    #for col in range(1,date_values.shape[1]):
    #    worksheet.write(0+7, col, date_values[1,col])         
    #
    #workbook.close() 
    
    
    #worksheet=workbook.add_worksheet('Dates')
    #for row, array in enumerate(date_values):   
    #    for col, value in enumerate(array):
    #        worksheet.write(row+7, col+2, value) 
    
     
    #data_values[0,0] =xx[1:10000,0]
    
    cur.close()
    con.close()
    return fund_names_l[0][0:7]