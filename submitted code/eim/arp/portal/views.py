from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template.context import RequestContext
from django.template.defaulttags import register
from django.forms.fields import CheckboxInput
from eim.arp.portal.forms import RiskParityForm
from django.contrib import messages
from dateutil.relativedelta import relativedelta
#import pdb
#pdb.set_trace()

@register.filter
def callMethod(obj, methodName):
	method = getattr(obj, methodName)

	if obj.__dict__.has_key("__callArg"):
		ret = method(*obj.__callArg)
		del obj.__callArg
		return ret
	return method()

@register.filter
def args(obj, arg):
	if not obj.__dict__.has_key("__callArg"):
		obj.__callArg = []

	obj.__callArg += [arg]
	return obj

def home(request):
    home = "Risk Parity Framework"
    return render_to_response(u'home.html', {'Home':home, }, context_instance=RequestContext(request))


def risk_parity_calc(request):
        
    import pandas as pd
    import win32com.client as wc
    import numpy as np
    from math import sqrt
    import xlsxwriter
    import xlwt
    import datetime
    from datetime import datetime,timedelta
    import calendar
    import xlrd
       
    def mean(lst):
        """calculates mean"""
        return sum(lst) / len(lst)
        
    def stddev(lst):
        """calculates standard deviation"""
        sum1 = 0
        mn = mean(lst)
        for i in range(len(lst)):
            sum1 += pow((lst[i]-mn),2)
        return sqrt(sum1 / (len(lst) - 1))
    
    def date_match(value,array,decider):
        '''matches date in a dates array-returns the position '''    
        diff=[None]*len(array)
        for i in range(0, len(array)):
            diff[i]=(array[i]-value).days
            
        if decider==1:#gives the date just greater than original date if original date is not available
            out=diff.index(min(x for x in diff if x>=0))
        elif decider==-1:#gives the date just below the original date
            out=diff.index(max(x for x in diff if x<=0))
            
        return out
    
    Period_Vol_Scaling = int(request.POST["Volatility_Scaling_Period_In_Months"])
    Data_Freq = request.POST["Data_Frequency"]
    Rebalancing_Freq = request.POST["Rebalancing_Frequency"]
    Target_Vol = float(request.POST["Target_Volatility_In_Percentage"])/100
    Max_Cap_Leverage_Portfolio = float(request.POST["Max_Cap_Leverage_PF"])/100
    Start_Date_Portflio = request.POST["Start_Date"]
    End_Date_Portflio = request.POST["End_Date"]
    Date_Lag=int(request.POST["Data_lag"])
        
    
    messages.success(request, 'Profile details updated.')
    inputfile=request.FILES['file']
    workbook=xlrd.open_workbook(file_contents=inputfile.read())
    worksheet=workbook.sheet_by_name('Data')
    num_rows=worksheet.nrows-1
    curr_row=0
    
    Max_Cap_Leverage_Asset=[None]*1000
    Trading_Costs=[None]*1000
    
    while curr_row<num_rows:
        curr_row+=1
        Max_Cap_Leverage_Asset[curr_row]=worksheet.cell(curr_row,0).value
        Trading_Costs[curr_row]=worksheet.cell(curr_row,1).value
        
    Max_Cap_Leverage_Asset=[x for x in Max_Cap_Leverage_Asset if x is not None]
    Trading_Costs=[x for x in Trading_Costs if x is not None]
        
    if Data_Freq=="Daily":
        Lag=Date_Lag
    else:
        Lag=0
    
    from Data2 import data
    from Data import fundnames
    Totaldata=data()
    AssetNames=fundnames()   
    
    tem=[x for x in Totaldata[:,0] if x is not None]
    num=len(tem)
    St_Dt_Port=datetime.strptime(Start_Date_Portflio,"%m/%d/%Y")
    End_Dt_Port=datetime.strptime(End_Date_Portflio,"%m/%d/%Y")
    for i in range(1,num+1):
        if St_Dt_Port==Totaldata[i,0]:
            st=i
        if End_Dt_Port==Totaldata[i,0]:
            end=i
    
    InitialData=np.array(Totaldata[st:(end+1)])
    
    Total_Assets=len([x for x in InitialData[InitialData.shape[0]-1] if x is not None]) #Number of columns
    for i in range(1,Total_Assets):
        tp=len(InitialData[:,i])-len([x for x in InitialData[:,i] if x is not None])
        InitialData[:,i][tp:len(InitialData[:,i])]=np.concatenate(InitialData[:,i][tp:len(InitialData[:,i])])[:,0]
    
    j=0
    Dates=[None]*10000
    AssetData=np.array([[None]*InitialData.shape[1]]*InitialData.shape[0])
    
    if Data_Freq=="Daily" or Data_Freq=="": # or indicates default freq i.e., daily
        AssetData=InitialData
    
    elif Data_Freq=="Weekly":
        
        Dates[0]=St_Dt_Port+timedelta(days=+4-St_Dt_Port.weekday())
        while Dates[j]<np.amax(InitialData[:,0])-timedelta(days=7):
            Dates[j+1]=Dates[j]+timedelta(days=+7)
            j=j+1
        Dates=[x for x in Dates if x is not None]
        for j in range(0,len(Dates)):
            for i in range(0,InitialData.shape[0]):
                AssetData[j]=InitialData[date_match(Dates[j],InitialData[:,0],1)]
    
    elif Data_Freq=="Monthly":
        for i in range(0,InitialData.shape[0]-1):
            if InitialData[:,0][i].month<>InitialData[:,0][i+1].month:
                Dates[j]=InitialData[:,0][i]
                AssetData[j]=InitialData[i]
                j=j+1
    
    if Data_Freq=="Daily":
        Total_Data_Each_Asset=AssetData.shape[0] 
    elif Data_Freq=="Monthly":
        AssetData[j]=InitialData[InitialData.shape[0]-1]
        Total_Data_Each_Asset=j+1
    else:
        AssetData[j]=InitialData[InitialData.shape[0]-1]
        Total_Data_Each_Asset=j+1
    #import pdb; pdb.set_trace()
    #Calculating Rebased values and daily returns
    Rebased_values=np.array([[None]*(Total_Assets)]*(Total_Data_Each_Asset)) #empty array for rebased values
    Daily_Returns=np.array([[None]*(Total_Assets)]*(Total_Data_Each_Asset-1)) #empty array for daily returns
    for z in range(1,Total_Data_Each_Asset+1):
        Rebased_values[z-1][0]=datetime(AssetData[z-1][0].year,AssetData[z-1][0].month,AssetData[z-1][0].day)
        Daily_Returns[z-2][0]=datetime(AssetData[z-1][0].year,AssetData[z-1][0].month,AssetData[z-1][0].day)
    
    
    for y in range(2,Total_Assets+1):
        rebaser=AssetData[len(AssetData[:,y-1][0:Total_Data_Each_Asset])-len([x for x in AssetData[:,y-1] if x is not None])][y-1]
        for z in range(1,Total_Data_Each_Asset+1):
            if AssetData[z-1][y-1]==None:
                Rebased_values[z-1][y-1]=0
            else:
                Rebased_values[z-1][y-1]=100*(AssetData[z-1][y-1]/rebaser)
           
    for y in range(2,Total_Assets+1):
       for z in range(2,Total_Data_Each_Asset+1):    
           if Rebased_values[z-2][y-1]==0:
               Daily_Returns[z-2][y-1]=0
           else:
               Daily_Returns[z-2][y-1]=(Rebased_values[z-1][y-1]/Rebased_values[z-2][y-1])-1
           
    
    #calculating the starting point based on the Vol Scaling Period
    b=Rebased_values[:,0][date_match(Rebased_values[0][0]+relativedelta(months=+int(Period_Vol_Scaling)),Rebased_values[:,0],-1)]
    for z in range(1, Total_Data_Each_Asset):
        if b== Rebased_values[:,0][z-1]:
            strt=z
    
    #import pdb; pdb.set_trace()
    Rolling_Vol=np.array([[None]*(Total_Assets)]*(Total_Data_Each_Asset))
    for z in range(2,Total_Data_Each_Asset+1):
       for y in range(2,Total_Assets+1):
           if z>=strt:
               Rolling_Vol[z-2][y-1]=np.std(Daily_Returns[:,y-1][z-(strt):(z-1)],ddof=1)*np.sqrt(252)
       
    #Calculating Weights' Rebalancing Dates
    #import pdb; pdb.set_trace()
    Quar_Rebal_Dates=[None]*10000
    Quar_Rebal_Dates[0]=b
    #import pdb; pdb.set_trace()
    if Rebalancing_Freq=="Quarterly" or Rebalancing_Freq=="Monthly" or Rebalancing_Freq=="":#or indicates default freq
        if Rebalancing_Freq=="Quarterly" or Rebalancing_Freq=="": # Quarterly or default
            t=3
        else: #monthly
            t=1   
        if b.month<>12:
            Quar_Rebal_Dates[1]=b+relativedelta(days=+(datetime(b.year,b.month+1,1)-b).days-1)
        else:
            Quar_Rebal_Dates[1]=b+relativedelta(days=+(datetime(b.year+1,1,1)-b).days-1)
        i=1
        while Quar_Rebal_Dates[i]<=np.amax(Rebased_values[:,0]):
            i=i+1
            Quar_Rebal_Dates[i]=Quar_Rebal_Dates[i-1]+relativedelta(months=+t)
            if Quar_Rebal_Dates[i].month==12:
                Quar_Rebal_Dates[i]=Quar_Rebal_Dates[i]+relativedelta(days=+(datetime(Quar_Rebal_Dates[i].year+1,1,1)-Quar_Rebal_Dates[i]).days-1)
            else:
                Quar_Rebal_Dates[i]=Quar_Rebal_Dates[i]+relativedelta(days=+(datetime(Quar_Rebal_Dates[i].year,Quar_Rebal_Dates[i].month+1,1)-Quar_Rebal_Dates[i]).days-1)
    
    elif Rebalancing_Freq=="Weekly":
        Quar_Rebal_Dates[1]=b+timedelta(days=7)
        i=1
        while Quar_Rebal_Dates[i]<=np.amax(Rebased_values[:,0]):
            i=i+1
            Quar_Rebal_Dates[i]=Quar_Rebal_Dates[i-1]+timedelta(days=7)
            
    elif Rebalancing_Freq=="Daily":
        Quar_Rebal_Dates=Rebased_values[:,0]
        
    Quar_Rebal_Dates[i]=np.amax(Rebased_values[:,0])
    Quar_Rebal_Dates=[x for x in Quar_Rebal_Dates if x is not None]
    Quar_Rebal_Dates=np.unique(Quar_Rebal_Dates)
    for i in range(0,len(Quar_Rebal_Dates)):
        indx=date_match(Quar_Rebal_Dates[i],Rebased_values[:,0],-1)
        Quar_Rebal_Dates[i]=Rebased_values[:,0][indx]
    
    #Calculating rebalancing weights and lagged weights and their indices.
    #import pdb; pdb.set_trace()
    Weights=np.array([[0]*Total_Assets]*Total_Data_Each_Asset,dtype=float)
    Lagged_weights=np.array([[0]*Total_Assets]*Total_Data_Each_Asset,dtype=float)
    Daily_Weights=np.array([[0]*Total_Assets]*Total_Data_Each_Asset,dtype=float)
    Daily_Basket_Return=np.array([0]*Total_Data_Each_Asset,dtype=float)
    
    for i in range(0,len(Quar_Rebal_Dates)):
        for j in range(1,Total_Data_Each_Asset):
            if Quar_Rebal_Dates[i]==Rebased_values[j,0]:
                for k in range(1,Total_Assets):
                    if Rolling_Vol[j-1][k]<>0 and Rolling_Vol[j-1][k]<>None:
                        Weights[j][k]=Target_Vol/Rolling_Vol[j-1][k]
                    else:
                        Weights[j][k]=0
    
    Quar_Lagged_Date_index=[None]*10000
    m=0
    for j in range(strt-1,Total_Data_Each_Asset):
        if (np.sum(Weights[j][1:len(Weights[j])]))!=0:
            for k in range(1,Total_Assets):    
                if j+Lag<Total_Data_Each_Asset: #----------------
                    Lagged_weights[j][k]=Weights[j][k]/(np.sum(Weights[j][1:len(Weights[j])]))
                    Daily_Weights[j+Lag][k]=Lagged_weights[j][k]
                    Quar_Lagged_Date_index[m]=j+Lag
                    m=m+1
    
    Quar_Lagged_Date_index=[x for x in Quar_Lagged_Date_index if x is not None]    
    Quar_Lagged_Date_index=np.unique(Quar_Lagged_Date_index)
    #Calculating Daily Weights
    for j in range(1,Total_Data_Each_Asset):
        if j<strt+Lag:
            Daily_Weights[j-1]=Daily_Weights[strt+Lag-1]
            Daily_Basket_Return[j]=np.dot(Daily_Weights[j-1][1:len(Daily_Weights[j-1])],Daily_Returns[j-1][1:len(Daily_Returns[j-1])])
        else:
            Daily_Basket_Return[j]=np.dot(Daily_Weights[j-1][1:len(Daily_Weights[j-1])],Daily_Returns[j-1][1:len(Daily_Returns[j-1])])
            for k in range(1,Total_Assets):
                if j in Quar_Lagged_Date_index:
                    Daily_Weights[j][k]=Daily_Weights[j][k]
                else:
                    Daily_Weights[j][k]=(Daily_Weights[j-1][k]*(1+Daily_Returns[j-1][k]))/(1+Daily_Basket_Return[j])
           
    Transaction_Cost=np.array([0]*Total_Data_Each_Asset,dtype=float)
    
    #Calculating transaction costs
    for j in range(1,Total_Data_Each_Asset): 
        if j<strt+2:
            Transaction_Cost[j]=0
        else:
            if (j-1) in Quar_Lagged_Date_index:
                Transaction_Cost[j]=-1*np.dot(abs((Daily_Weights[j-2][1:len(Daily_Weights[j-2])]-Daily_Weights[j-1][1:len(Daily_Weights[j-1])])),np.array(Trading_Costs))
            else:
                Transaction_Cost[j]=0
    
    Index_ER=np.array([0]*(Total_Data_Each_Asset-strt-Lag+1),dtype=float)
    Top_lvl_Vol=np.array([0]*(Total_Data_Each_Asset-strt-Lag+1),dtype=float)
    Scaled_Vol=np.array([0]*(Total_Data_Each_Asset-strt-Lag+1),dtype=float)
    Equity=np.array([0]*(Total_Data_Each_Asset-strt-Lag+1),dtype=float)
    Loan=np.array([0]*(Total_Data_Each_Asset-strt-Lag+1),dtype=float)
    Final_Index_ER=np.array([0]*(Total_Data_Each_Asset-strt-Lag+1),dtype=float)
    Final_Index_Daily_Returns=np.array([0]*(Total_Data_Each_Asset-strt-Lag+1),dtype=float)
    Daily_Vol=np.array([0]*(Total_Data_Each_Asset-strt-Lag+1),dtype=float)
    
    #Calculating the final index series
    for x in range(0,Total_Data_Each_Asset-strt-Lag+1):
        if x==0:
            Index_ER[0]=100
            Final_Index_ER[0]=100
        else:
            Index_ER[x]=Index_ER[x-1]*(1+Daily_Basket_Return[x+strt+Lag-1]+Transaction_Cost[x+strt+Lag-1])
        
        if (x+strt+Lag-1) in Quar_Lagged_Date_index:
            Top_lvl_Vol[x]=np.std(Daily_Basket_Return[x+strt+Lag-1-(strt-2):(x+strt+Lag)],ddof=1)*np.sqrt(252)
            Scaled_Vol[x]=min(Max_Cap_Leverage_Portfolio,Target_Vol/Top_lvl_Vol[x])
            
        if x==0:
            Equity[x]=Scaled_Vol[x]*Final_Index_ER[0]
            Loan[x]=(Scaled_Vol[x]-1)*Final_Index_ER[x]
        else:
            if (x+strt+Lag-2) in Quar_Lagged_Date_index:
                Equity[x]=Final_Index_ER[x-1]*Scaled_Vol[x-1]*(1+Daily_Basket_Return[x+strt+Lag-1]+Transaction_Cost[x+strt+Lag-1])+(abs(Equity[x-1]-(Scaled_Vol[x-1]*Final_Index_ER[x-1])))*(Transaction_Cost[x+strt+Lag-1])
                Loan[x]=(Scaled_Vol[x-1]-1)*Final_Index_ER[x-1]
                Final_Index_ER[x]=Equity[x]-Loan[x]
            else:
                Equity[x]=Equity[x-1]*(1+Daily_Basket_Return[x+strt+Lag-1]+Transaction_Cost[x+strt+Lag-1])
                Loan[x]=Loan[x-1]
                Final_Index_ER[x]=Equity[x]-Loan[x]
    
    
    for x in range(1,Total_Data_Each_Asset-strt-Lag+1):
        Final_Index_Daily_Returns[x]=Final_Index_ER[x]/Final_Index_ER[x-1]-1
        if x>=strt:
            Daily_Vol[x]=stddev(Final_Index_Daily_Returns[x-strt+2:x+1])*sqrt(252)
        if Scaled_Vol[x]==0:
            Scaled_Vol[x]=Scaled_Vol[x-1]
        else:
            Scaled_Vol[x]=Scaled_Vol[x]  
    
    Output_Weights=np.array([[None]*(Total_Assets)]*(Total_Data_Each_Asset-strt-Lag+1),dtype=float)
    for y in range(0,Total_Assets):
        for x in range(0, Total_Data_Each_Asset-strt-Lag+1):
            Output_Weights[x][y]=Scaled_Vol[x]*Daily_Weights[x+strt+Lag-1][y]
    
#    Lagged_dates=[None]*10000
#    for i in range(0,len(Rebased_values[:,0][(strt+Lag-1):(len(Rebased_values))])):
#        Lagged_dates[i]=1
    Performance_Indicators=np.array([[None]*(Total_Assets+1)]*(Total_Data_Each_Asset-strt-Lag+1))
    Performance_Indicators[:,0]=Rebased_values[:,0][(strt+Lag-1):(len(Rebased_values))]
    Performance_Indicators[:,1]=Final_Index_Daily_Returns
    Performance_Indicators[:,2]=Daily_Vol
    Performance_Indicators[:,3]=Transaction_Cost[(strt+Lag-1):(len(Transaction_Cost))]
    Performance_Indicators[:,4]=Scaled_Vol
    
    
    #Generating Output in Excel
    
    workbook=xlsxwriter.Workbook('Output.xlsx')
    worksheet=workbook.add_worksheet('Results')
    
    format1 = workbook.add_format({
    'align': 'center',
    'valign': 'vcenter',
    'fg_color': 'white'
    })
    format1.set_num_format('0.00%')
    worksheet.set_column('A:B', 4, format1)
    worksheet.set_column('C:G', 12, format1)
    worksheet.set_column('H:I', 4, format1)
    worksheet.set_column('J:R', 12, format1)
    
    
    format2 = workbook.add_format({
    'bold': True,
    'align': 'center',
    'valign': 'vcenter',
    'fg_color': '8b4513',
    'font_color': 'white'
    })
    
    
    worksheet.merge_range('C4:E4', 'Average Rebalancing Trading Cost', format2)
    worksheet.merge_range('C6:G6', 'Performance of Cluster/ Portfolio', format2)
    worksheet.merge_range('J6:K6', 'Weights of Assets', format2)
    
    worksheet.set_row(3, 21)
    worksheet.set_row(5, 21)
    
    for row, array in enumerate(Performance_Indicators[1:len(Performance_Indicators)]):   
        for col, value in enumerate(array):
            if col==0:
                format3  = workbook.add_format({
                        'align': 'center',
                        'valign': 'vcenter',
                        'fg_color': 'white'
                    })
                format3.set_num_format('dd-mmm-yy')
                worksheet.write(row+7, col+2, value, format3)
            else:        
                worksheet.write(row+7, col+2, value, format1)
        
    
    for row, array in enumerate(Output_Weights[1:Output_Weights.shape[0],1:Output_Weights.shape[1]]):   
        for col, value in enumerate(array):
            worksheet.write(row+7, col+9, value, format1)


    format2 = workbook.add_format({
    'bold': True,
    'align': 'center',
    'valign': 'vcenter',
    'fg_color': 'f5deb3',
    'font_color': 'black'
    })
    
    format2.set_text_wrap()
    worksheet.merge_range('F4:G4','', format2)
    worksheet.write('C7', 'Dates', format2)
    worksheet.write('D7', 'Daily_Return', format2)
    worksheet.write('E7', 'Vol', format2)
    worksheet.write('F7', 'Trading Cost', format2)
    worksheet.write('G7', 'Leverage', format2)
    for i in range(9,Total_Assets+8):
        worksheet.write(6,i, str(AssetNames[i-9]), format2)
    
    workbook.close()

def risk_parity(request):   

    if request.method == 'POST':
        form = RiskParityForm(request.POST, request.FILES)
		# If form is valid
        if form.is_valid():
            #form.save()
            risk_parity_calc(request)
            return redirect(u'/')
    else:
		# Make new form
	  form = RiskParityForm()
    return render_to_response(u'risk_parity.html', {'form': form}, context_instance=RequestContext(request))

####From here started by TarunS

@register.filter(name='is_checkbox')
def is_checkbox(value):
    return isinstance(value, CheckboxInput)

#@decorator
def screen2(request,field=[]):
    filter_names_dict = { 'STYLE' : 'cf.newstyle' , 'PROVIDER' : "SUBSTR(fund_lw, 1, INSTR(fund_lw, ' ')-1)" , 'STRATEGY' : 'cf.newstrategy' , 'METHODOLOGY' : 'cf.newmethodology' , 'ASSET_CLASS' : 'cf.newassetclass' , 'CLASSIFICATION_GROUP' : 'cf.newclassgrouplw' }
    print "iam herexxx"
    cur=connection()
    print "iam here2"
    final_dict={}

    sql = """
SELECT f.fund_lw AS fund_name, SUBSTR(fund_lw, 1, INSTR(fund_lw, ' ')-1) AS provider,
cf.newstyle AS style, cf.newstrategy AS strategy, cf.newmethodology AS methodology,
cf.newassetclass AS asset_class,cf.newclassgrouplw AS classification_group
FROM v_arp_funds f,v_eim_classification cf WHERE f.fund_id = cf.fund_id
    """
    """for key,value in field.items():
        items = "','".join(value)
        sql += " and %s in ('%s') " % (filter_names_dict[key],items)
    
    print sql
    cur.execute(sql)
    data = [ values for values in cur.fetchall() if values ]
    print type(data)
    print data        
    field=data
    #field.extend(data[0])
    """
    if request.method == 'POST':
        return render_to_response(u'screen2.html', {'field': field,'query': sql}, context_instance=RequestContext(request))
    elif request.method == 'BACK':
        pass
    import json
    from django.template.loader import render_to_string
    if request.is_ajax():
        html=render_to_response(u'screen2.html', {'field': sql,'query': sql})
        return HttpResponse(json.dumps(filter_names_dict))
    return render_to_response(u'screen2.html', {'field': field,'query': sql}, context_instance=RequestContext(request))

def connection():
    import cx_Oracle
    HOST = 'sump.eimad.local'                   
    PORT = '1521'
    SID = 'testdb'
    
    dsn_1 = cx_Oracle.makedsn(HOST, PORT , SID)
    con = cx_Oracle.connect('I_JCPLESSIS' ,'JCPLESSIS',dsn_1)
    return con.cursor()

#@decorator
def initial_checks(request):
    #from Screen1_DB import connection as conn
    cur=connection()
    final_dict={}
    filter_names_dict = { 'STYLE' : 'cf.newstyle' , 'PROVIDER' : "SUBSTR(fund_lw, 1, INSTR(fund_lw, ' ')-1)" , 'STRATEGY' : 'cf.newstrategy' , 'METHODOLOGY' : 'cf.newmethodology' , 'ASSET_CLASS' : 'cf.newassetclass' , 'CLASSIFICATION_GROUP' : 'cf.newclassgrouplw' }
    filter_names = ['STYLE' , 'PROVIDER' , 'STRATEGY'  , 'METHODOLOGY'  , 'asset_class' , 'classification_group' ]
    
    for filter_name in filter_names:
        data=[]
        sql="""
SELECT 
distinct(%s)
FROM v_arp_funds f,v_eim_classification cf
WHERE f.fund_id = cf.fund_id
        """ % filter_names_dict[str(filter_name).upper()]
        cur.execute(sql)
        data = [ values[0] for values in cur.fetchall() if values[0] ]
        if data:
            final_dict[str(filter_name).upper()] = data
        
    field=final_dict
    
    
    

    
    if request.method == 'POST':
        assest_dict={}
        for i in final_dict.keys():
            key = "checks_" + str(i).upper()
            if request.POST.getlist(key):
                assest_dict[i] = request.POST.getlist(key)
        return screen2(request,assest_dict)
        #return render_to_response(u'screen1.html', {'field': request.POST['Get_Assests]}, context_instance=RequestContext(request))
    return render_to_response(u'screen1.html', {'field': field}, context_instance=RequestContext(request))
    
    
def create_post(request):
    if request.method == 'POST':
        post_text = request.POST.get('the_post')
        response_data = {}

        response_data['result'] = 'Create post successful!'
        response_data['postpk'] = "2"
        response_data['text'] = "23"
        response_data['created'] = "24"
        response_data['author'] = "25"
        import json
        print "iam here"
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )
    else:
        print "iam here"
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )