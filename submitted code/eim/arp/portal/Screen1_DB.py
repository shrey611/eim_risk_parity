# -*- coding: utf-8 -*-
"""
Created on Wed Jul 01 12:35:18 2015

@author: sjain
"""

import cx_Oracle

def connection():

    HOST = 'sump.eimad.local'                   
    PORT = '1521'
    SID = 'testdb'
    
    dsn_1 = cx_Oracle.makedsn(HOST, PORT , SID)
    con = cx_Oracle.connect('I_JCPLESSIS' ,'JCPLESSIS',dsn_1)
    return con.cursor()