from django.db import connections

# Not stored in DB (only IDS are stored)
class Single:
	def __init__(self, id, name):
		self.id = id
		self.name = name

	def __str__(self):
		return self.name

def get_singles():
	cursor = connections['intranet'].cursor()
	cursor.execute("SELECT * FROM V_ARP_FUNDS ORDER BY FUND_LW")
	desc = cursor.description

	dicts = [
		dict(zip([col[0] for col in desc], row))
		for row in cursor.fetchall()
	]
	return [
		Single(int(s["FUND_ID"]), s["FUND_LW"]) for s in dicts
	]

def get_single(single_id):
	cursor = connections['intranet'].cursor()
	cursor.execute("SELECT * FROM V_ARP_FUNDS WHERE FUND_ID = %s ORDER BY FUND_LW", [single_id, ])
	desc = cursor.description

	dicts = [
		dict(zip([col[0] for col in desc], row))
		for row in cursor.fetchall()
	]
	return Single(int(dicts[0]["FUND_ID"]), dicts[0]["FUND_LW"])


def get_perfs(single_intranet_id):
	cursor = connections['intranet'].cursor()
	cursor.execute("SELECT VALUE_DAT, VALUE_VAL FROM V_ARP_PERFS WHERE FUND_ID = %s ORDER BY VALUE_DAT", [single_intranet_id, ])
	desc = cursor.description

	return [	dict(zip([col[0] for col in desc], row))
		for row in cursor.fetchall()
	]
