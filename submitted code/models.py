from django.db import models
from django.utils import timezone
from eim.arp.portal.widgets import MultiSelectField


# Static data
DATA_FREQUENCIES = (
	('M', 'Monthly'),
	('W', 'Weekly'),
)

REBALANCING_FREQUENCIES = (
	('M', 'Monthly'),
	('Q', 'Quarterly'),
)

METHODOLOGIES = (
	(1, 'Omega maximization'),
	(2, 'Volatility minimization'),
	(3, 'Drawdown minimization'),
	(4, 'CVaR minimization'),
)


class Entity(models.Model):
	'''
	Define an hyperclass entity which is the base for a portfolio, a cluster or an underlying security
	'''
	
	# User-defined identifier
	name = models.CharField(max_length=255, unique=True)
	# Last update date
	update_datetime = models.DateTimeField(db_column=u'update_datetime', blank=True, default=timezone.now())
	
	class Meta:
		db_table = 'entity'
		verbose_name = 'Entity'
		verbose_name_plural = 'Entities'


class Cluster(Entity):
	'''
	Define a cluster
	'''
	
	# List of Intranet IDs
	single_ids = MultiSelectField(max_length=1024)
	
	class Meta:
		db_table = 'ARP_cluster'
		verbose_name = 'Cluster'
		verbose_name_plural = 'Clusters'


class OneShotEntity(models.Model):
	'''
	Define a one-shot entity model which is the base for the components of a portfolio
	'''
	
	# User-defined identifier
	name = models.CharField(max_length=255)
	
	class Meta:
		db_table = 'one_shot_entity'
		verbose_name = 'OneShotEntity'
		verbose_name_plural = 'OneShotEntities'


class Portfolio(Entity):
	'''
	Define a portfolio, which may contain clusters and/or singles
	'''
	
	# Underlying clusters
	components = models.ManyToManyField(OneShotEntity, related_name=u'portfolios_components', blank=True)
	
	def __str__(self):
		return self.name
	
	class Meta:
		db_table = 'portfolio'
		verbose_name = 'Portfolio'
		verbose_name_plural = 'Portfolios'


class TimeSerie(models.Model):
	'''
	Define a time serie
	'''
	
	# Frequency
	frequency = models.CharField(db_column=u'data_frequency', max_length=3, choices=DATA_FREQUENCIES)
	# Related entity
	entity = models.ForeignKey(OneShotEntity)
	
	def get_all_time_serie_values(self):
		return TimeSerieValue.objects.filter(time_serie=self).order_by('date')
	
	def get_time_serie_values(self, start_date=None, end_date=None):
		'''
		Get all time series values associated to this time series within given range (inclusive)
		'''
		
		# Check input
		if start_date is None and end_date is None:
			# Return everything
			return self.get_all_time_serie_values()
		elif start_date is None:
			# Return everything before end date
			return TimeSerieValue.objects.filter(time_serie=self, date__lte=end_date).order_by('date')
		elif end_date is None:
			# Return everything after start date
			return TimeSerieValue.objects.filter(time_serie=self, date__gte=start_date).order_by('date')
		else:
			return TimeSerieValue.objects.filter(time_serie=self, date__range=(start_date, end_date)).order_by('date')
	
	class Meta:
		db_table = 'time_serie'
		verbose_name = 'TimeSerie'
		verbose_name_plural = 'TimeSeries'


class TimeSerieValue(models.Model):
	'''
	Define a time serie value
	'''
	
	# Related time serie
	time_serie = models.ForeignKey(TimeSerie)
	date = models.DateField(db_column=u'application_date')
	value = models.FloatField(db_column=u'ts_value')
	
	class Meta:
		db_table = 'time_serie_value'
		verbose_name = 'TimeSerieValue'
		verbose_name_plural = 'TimeSerieValues'


class Backtest(models.Model):
	'''
	Define a back-test scenario: portfolio content, methodology, etc.
	'''
	
	# User-defined identifier
	name = models.CharField(max_length=255, unique=True)
	# Last update date
	update_datetime = models.DateTimeField(db_column=u'update_datetime', blank=True, default=timezone.now())
	
	# Portfolio
	portfolio = models.ForeignKey(Portfolio)
	
	# Input performance tracks frequency
	data_frequency = models.CharField(db_column=u'data_frequency', max_length=3, choices=DATA_FREQUENCIES)
	# TODO: Cost integration
	# Depth of look back horizon
	look_back_depth = models.IntegerField(db_column=u'look_back_depth', blank=True)
	# Start date
	start_date = models.DateField(db_column=u'start_date')
	# End date
	end_date = models.DateField(db_column=u'end_date')
	# Rebalancing frequency
	rebalancing_frequency = models.CharField(db_column=u'rebalancing_frequency', max_length=3, choices=REBALANCING_FREQUENCIES)
	# Number of simulations
	number_of_simulations = models.IntegerField(db_column=u'number_of_simulations', default=36000)
	# Seed for simulations
#	seed = models.IntegerField(db_column=u'seed', blank=True)
	# Methodology
	methodology = models.IntegerField(choices=METHODOLOGIES)
	# Target return (and frequency - yearly to start with) for Omega threshold computation
	yearly_target_return = models.FloatField(blank=True, default=0.1)
	# CVaR confidence level
	cvar_confidence_level = models.FloatField(blank=True, default=0.083333)
	
	class Meta:
		db_table = 'backtest'
		verbose_name = 'Backtest'
		verbose_name_plural = 'Backtests'
	
class WeightConstraint(models.Model):
	'''
	Define weight constraints (min/max) on an entity
	'''
	
	# Back-test for which it is defined
	back_test = models.ForeignKey(Backtest)
	# Related entity
	entity = models.ForeignKey(OneShotEntity)
	
	# Minimum weight
	min = models.FloatField()
	# Maximum weight
	max = models.FloatField()
	
	class Meta:
		db_table = 'weight_constraint'
		verbose_name = 'WeightConstraint'
		verbose_name_plural = 'WeightConstraints'
	
class OptimizationResult(models.Model):
	'''
	Keep the results of optimizations
	'''
	
	# Back-test for which it is defined
	back_test = models.ForeignKey(Backtest)
	# Related entity
	entity = models.ForeignKey(OneShotEntity)
	# Application date
	date = models.DateField(db_column=u'application_date')
	# Optimal weight
	weight = models.FloatField()
	# Last update date
	computation_datetime = models.DateTimeField(db_column=u'computation_datetime', blank=True, default=timezone.now())
	
	class Meta:
		db_table = 'optimization_result'
		verbose_name = 'OptimizationResult'
		verbose_name_plural = 'OptimizationResults'
	
