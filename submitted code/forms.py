from django import forms

from functools import partial
DateInput = partial(forms.DateInput, {'class': 'datepicker'})

# Static data
DATA_FREQUENCIES = ( 
      ('Monthly', 'Monthly'),
	('Weekly', 'Weekly'),
      ('Daily', 'Daily'),
)

REBALANCING_FREQUENCIES = (
	('Daily', 'Daily'),
	('Weekly', 'Weekly'),
	('Monthly', 'Monthly'),
	('Quarterly', 'Quarterly'),
)

METHODOLOGIES = (
	(1, 'Omega maximization'),
	(2, 'Volatility minimization'),
	(3, 'Drawdown minimization'),
	(4, 'CVaR minimization'),
)

VOL_SCALING_PD = (
	(1, '1 Month'),
	(3, '3 Month'),
	(6, '6 Month'),
	(12, '12 Month'),
)



class RiskParityForm(forms.Form):
	Volatility_Scaling_Period_In_Months = forms.ChoiceField(choices=VOL_SCALING_PD) 
	Data_Frequency = forms.ChoiceField(choices=DATA_FREQUENCIES)
	Rebalancing_Frequency = forms.ChoiceField(choices=REBALANCING_FREQUENCIES)
	Target_Volatility_In_Percentage = forms.FloatField()
	Data_lag = forms.IntegerField()
	Max_Cap_Leverage_PF = forms.IntegerField()
	# Start date
	Start_Date = forms.CharField(widget=DateInput())
	# End date
	End_Date =  forms.CharField(widget=DateInput())
 	file = forms.FileField()

