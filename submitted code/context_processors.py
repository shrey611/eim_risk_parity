def settings_context(request):
    from django.conf import settings
    
    dict = {
        'PROD': settings.PROD,
        'VERSION': settings.VERSION,
    }
    return dict